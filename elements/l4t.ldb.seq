!++HEAD!++
 /************************************************************************************************************
 *
 * Linac 4, L4T transfer Line version STUDY in MAD X SEQUENCE format
 * Generated the 18-MAY-2020 13:19:53 from LHCLAYOUT@EDMSDB Database
 * OPTIC  : L4T Line
 * Linac 4 default optic from L4T.VVGS.0101 to LT.BHZ20.E
 *
 ************************************************************************************************************/
!++END_HEAD!+++

!++ELEMENTS!++

/************************************************************************************************************/
/*                       TYPES DEFINITION                                                                   */
/************************************************************************************************************/

//---------------------- INSTRUMENT     ---------------------------------------------------------------------
BWS__002  : INSTRUMENT      , L := 0;         ! Wire Scanners Profile Monitors
TKSTR     : INSTRUMENT      , L := 0;         ! PSB Kickers - Targets:  Stripping Foil Assembly
//---------------------- KICKER         ---------------------------------------------------------------------
MCHAFCAP  : KICKER          , L := 0;         ! Corrector magnet, horizontal, LINAC4 laser electron monitor
MCHV_001  : KICKER          , L := 0;         ! Corrector magnet, H+V steering, 120mm, L4 transfer line or Linac4 - Length:.12
//---------------------- MARKER         ---------------------------------------------------------------------
L4TOMK    : MARKER          , L := 0;         ! Linac 4 T Markers for the MAD File
VVGAB     : MARKER          , L := 0;         ! Vacuum valve gate elastomer pneumatic - DN63CF
VVGAC     : MARKER          , L := 0;         ! Vacuum valve gate elastomer pneumatic - DN100CF
//---------------------- MATRIX         ---------------------------------------------------------------------
ACDB      : MATRIX          , L := 0;         ! Debunching cavity
//---------------------- MONITOR        ---------------------------------------------------------------------
BCT__112  : MONITOR         , L := 0;         ! Beam current transformer, L4L
BCT__182  : MONITOR         , L := 0;         ! Beam current transformer, L4T
BEL__001  : MONITOR         , L := 0;         ! Beam Emittance - Laser Station
BEP__001  : MONITOR         , L := 0;         ! Beam Emittance Profile - Electron Detector
BPLFS     : MONITOR         , L := 0;         ! Longitudinal Bunch Profile monitor Feshenko
BPLFS002  : MONITOR         , L := 0;         ! Longitudinal Bunch Profile monitor Feshenko
BPUSE     : MONITOR         , L := 0;         ! Beam Position Pick-up, Stripline, type E
BSGHV001  : MONITOR         , L := 0;         ! SEM Grid (secondary emission monitor) Horizontal and Vertical
//---------------------- QUADRUPOLE     ---------------------------------------------------------------------
MQD__105  : QUADRUPOLE      , L := 0.105;     ! Quadrupole Magnet - Defocusing - Length:.105
MQD__300  : QUADRUPOLE      , L := 0.300;     ! Quadrupole Magnet - Defocusing - Length:.3
MQF__105  : QUADRUPOLE      , L := 0.105;     ! Quadrupole Magnet - Focusing - Magnetic Length:.105
MQF__300  : QUADRUPOLE      , L := 0.300;     ! Quadrupole Magnet - Focusing - Magnetic Length:.3
//---------------------- RCOLLIMATOR    ---------------------------------------------------------------------
TDISA     : RCOLLIMATOR     , L := 0;         ! Beam Dump, Internal, with beam Stopper functionality, Assembly
//---------------------- SBEND          ---------------------------------------------------------------------
MBH       : SBEND           , L := 0.870;     ! Horizontal Bending Magnets
MBV       : SBEND           , L := 0.870;     ! Vertical Bending Magnets

!return;
!++END_ELEMENTS!++

!++SEQUENCE!++

/************************************************************************************************************/
/*                       L4T                                                                                */
/************************************************************************************************************/

!L4T : SEQUENCE, REFER = centre,    L = 71.47095226;
L4T : SEQUENCE, REFER = centre,                             L = 70.12612;
! The start is defined as the end of the PIMS
! MP= Mechanical Position of an element

 L4T.BEGL4T.0000     : L4TOMK       , AT = 0                 , SLOT_ID = 5542740;                                   !MP= 0
 L4T.VVGS.0101       : VVGAB        , AT = .155              , SLOT_ID = 5528966;                                   !MP= 0.1550
 L4T.MCHV.0105       : MCHV_001     , AT = .38               , SLOT_ID = 5528968;                                   !MP= 0.38
 L4T.BCT.0107        : BCT__112     , AT = .725              , SLOT_ID = 5528967;                                   !MP= 0.7250
 L4T.MQD.0110        : MQD__105     , AT = 1.1               , SLOT_ID = 5528969;                                   !MP= 1.10
 L4T.BEL.0112        : BEL__001     , AT = 1.5834            , SLOT_ID = 42550988;                                  !MP= 1.5834
 L4T.BEP.0113        : BEP__001     , AT = 1.7812            , SLOT_ID = 42550995;                                  !MP= 1.7812
 L4T.MCH.0113        : MCHAFCAP     , AT = 1.7812            , SLOT_ID = 42550998;                                  !MP= 1.7812
 L4T.MCHV.0115       : MCHV_001     , AT = 2.1               , SLOT_ID = 5528970;                                   !MP= 2.10
 L4T.TKSTR.0124      : TKSTR        , AT = 2.565             , SLOT_ID = 14302971;                                  !MP= 2.5650
 L4T.MCHV.0135       : MCHV_001     , AT = 3.1               , SLOT_ID = 5528971;                                   !MP= 3.10
 L4T.VVGS.0201       : VVGAB        , AT = 3.81              , SLOT_ID = 14302989, ASSEMBLY_ID = 34467433;          !MP= 3.81
 L4T.MQF.0210        : MQF__105     , AT = 4.1               , SLOT_ID = 5528972, ASSEMBLY_ID = 34467433;           !MP= 4.10
 L4T.BSGHV.0223      : BSGHV001     , AT = 4.299             , SLOT_ID = 5528973, ASSEMBLY_ID = 34467433;           !MP= 4.2990
 L4T.BWS.0223        : BWS__002     , AT = 4.342             , SLOT_ID = 36269851, ASSEMBLY_ID = 34467433;          !MP= 4.3420
 L4T.BPM.0227        : BPUSE        , AT = 4.7323            , SLOT_ID = 5528974;                                   !MP= 4.7323
 L4T.BPLFS.0233      : BPLFS        , AT = 5.55              , SLOT_ID = 5528975;                                   !MP= 5.55
 L4T.BPM.0237        : BPUSE        , AT = 6.2323            , SLOT_ID = 5528976, ASSEMBLY_ID = 34467434;           !MP= 6.2323
 L4T.BSGHV.0243      : BSGHV001     , AT = 6.609             , SLOT_ID = 5528977, ASSEMBLY_ID = 34467434;           !MP= 6.6090
 L4T.BWS.0243        : BWS__002     , AT = 6.652             , SLOT_ID = 36269854, ASSEMBLY_ID = 34467434;          !MP= 6.6520
 L4T.MBH.0250        : MBH          , AT = 7.46936           , SLOT_ID = 5528978;                                   !MP= 7.475520
 L4T.MQD.0310        : MQD__300     , AT = 8.96321           , SLOT_ID = 5528979;                                   !MP= 8.975520
 L4T.MCHV.0315       : MCHV_001     , AT = 9.51321           , SLOT_ID = 5528980;                                   !MP= 9.525520
 L4T.MQF.0410        : MQF__300     , AT = 10.06321          , SLOT_ID = 5528981;                                   !MP= 10.075520
 L4T.BEL.0422        : BEL__001     , AT = 10.40041          , SLOT_ID = 42551205;                                  !MP= 10.412720
 L4T.BEP.0428        : BEP__001     , AT = 10.59821          , SLOT_ID = 42551310;                                  !MP= 10.610520
 L4T.MCH.0428        : MCHAFCAP     , AT = 10.59821          , SLOT_ID = 42551307;                                  !MP= 10.610520
 L4T.MBH.0450        : MBH          , AT = 11.55705          , SLOT_ID = 5528982;                                   !MP= 11.575520
 L4T.MQF.0510        : MQF__300     , AT = 13.0509           , SLOT_ID = 5528983;                                   !MP= 13.075520
 L4T.BSGHV.0523      : BSGHV001     , AT = 13.5799           , SLOT_ID = 5528984;                                   !MP= 13.604520
 L4T.BWS.0523        : BWS__002     , AT = 13.6229           , SLOT_ID = 36269855;                                  !MP= 13.647520
 L4T.MQD.0610        : MQD__300     , AT = 14.1509           , SLOT_ID = 5528985;                                   !MP= 14.175520
 L4T.MBH.0650        : MBH          , AT = 15.64474          , SLOT_ID = 5528986;                                   !MP= 15.675520
 L4T.BCT.0673        : BCT__182     , AT = 17.02859          , SLOT_ID = 5528987;                                   !MP= 17.065520
 L4T.MQF.0710        : MQF__300     , AT = 17.63859          , SLOT_ID = 5528988;                                   !MP= 17.675520
 L4T.MCHV.0715       : MCHV_001     , AT = 18.33859          , SLOT_ID = 5528989;                                   !MP= 18.375520
 L4T.MCHV.0735       : MCHV_001     , AT = 19.33859          , SLOT_ID = 5528990;                                   !MP= 19.375520
 L4T.TDISA.0740      : TDISA        , AT = 21.26159          , SLOT_ID = 5528992;                                   !MP= 21.298520
 L4T.MQD.0810        : MQD__300     , AT = 22.93859          , SLOT_ID = 5528993;                                   !MP= 22.975520
 L4T.BPM.0827        : BPUSE        , AT = 25.57094          , SLOT_ID = 5528994;                                   !MP= 25.607870
 L4T.BPM.0837        : BPUSE        , AT = 27.07094          , SLOT_ID = 5528995;                                   !MP= 27.107870
 L4T.MQF.0910        : MQF__300     , AT = 28.23859          , SLOT_ID = 5528996;                                   !MP= 28.275520
 L4T.MCHV.0915       : MCHV_001     , AT = 28.93859          , SLOT_ID = 5528997;                                   !MP= 28.975520
 L4T.MCHV.0935       : MCHV_001     , AT = 29.93859          , SLOT_ID = 5528998;                                   !MP= 29.975520
 L4T.MQD.1010        : MQD__300     , AT = 33.53859          , SLOT_ID = 5528999;                                   !MP= 33.575520
 L4T.BPM.1027        : BPUSE        , AT = 36.47094          , SLOT_ID = 5529001;                                   !MP= 36.507870
 L4T.BPM.1037        : BPUSE        , AT = 37.97094          , SLOT_ID = 5529002;                                   !MP= 38.007870
 L4T.BCT.1043        : BCT__112     , AT = 40.36859          , SLOT_ID = 5529003;                                   !MP= 40.405520
 L4T.ACDB.1075       : ACDB         , AT = 41.53859          , SLOT_ID = 5529004;                                   !MP= 41.575520
 L4T.VVGS.1081       : VVGAC        , AT = 42.52098          , SLOT_ID = 6709507;                                   !MP= 42.557910
 L4T.MCHV.1085       : MCHV_001     , AT = 43.53859          , SLOT_ID = 5529006;                                   !MP= 43.575520
 L4T.MCHV.1095       : MCHV_001     , AT = 44.53859          , SLOT_ID = 5529007;                                   !MP= 44.575520
 L4T.MQD.1110        : MQD__300     , AT = 45.23859          , SLOT_ID = 5529008;                                   !MP= 45.275520
 L4T.MQF.1210        : MQF__300     , AT = 46.48859          , SLOT_ID = 5529009;                                   !MP= 46.525520
 L4T.BPM.1227        : BPUSE        , AT = 47.07094          , SLOT_ID = 5529010;                                   !MP= 47.107870
 L4T.BCT.1243        : BCT__182     , AT = 48.29859          , SLOT_ID = 5529015, ASSEMBLY_ID = 34467435;           !MP= 48.335520
 L4T.BPM.1245        : BPUSE        , AT = 48.57094          , SLOT_ID = 5529016, ASSEMBLY_ID = 34467435;           !MP= 48.607870
 L4T.BSGHV.1247      : BSGHV001     , AT = 49.2226           , SLOT_ID = 5529017;                                   !MP= 49.259530
 L4T.BWS.1247        : BWS__002     , AT = 49.2656           , SLOT_ID = 36269856;                                  !MP= 49.302530
 L4T.MBV.1250        : MBV          , AT = 50.48625          , SLOT_ID = 5529018;                                   !MP= 50.525520
 L4T.MQD.1310        : MQD__300     , AT = 52.98392          , SLOT_ID = 5529019;                                   !MP= 53.025520
 L4T.MCHV.1315       : MCHV_001     , AT = 54.23392          , SLOT_ID = 5529020;                                   !MP= 54.275520
 L4T.MQF.1410        : MQF__300     , AT = 55.48392          , SLOT_ID = 5529021;                                   !MP= 55.525520
 L4T.MCHV.1415       : MCHV_001     , AT = 56.73392          , SLOT_ID = 5529022;                                   !MP= 56.775520
 L4T.MQD.1510        : MQD__300     , AT = 57.98392          , SLOT_ID = 5529023;                                   !MP= 58.025520
 L4T.MBV.1550        : MBV          , AT = 60.48158          , SLOT_ID = 5529025;                                   !MP= 60.525520
 L4T.BCT.1553        : BCT__182     , AT = 61.62925          , SLOT_ID = 5529026, ASSEMBLY_ID = 34467436;           !MP= 61.675520
 L4T.BPM.1557        : BPUSE        , AT = 61.9216           , SLOT_ID = 5529027, ASSEMBLY_ID = 34467436;           !MP= 61.967870
 L4T.MCHV.1605       : MCHV_001     , AT = 62.33123          , SLOT_ID = 5529030, ASSEMBLY_ID = 34467437;           !MP= 62.3775
 L4T.MQD.1610        : MQD__300     , AT = 62.67925          , SLOT_ID = 5529028, ASSEMBLY_ID = 34467437;           !MP= 62.725520
 L4T.BPM.1627        : BPUSE        , AT = 62.9616           , SLOT_ID = 5529029, ASSEMBLY_ID = 34467437;           !MP= 63.007870
 L4T.BPLFS.1663      : BPLFS002     , AT = 63.46925          , SLOT_ID = 6611797;                                   !MP= 63.515520
 L4T.MQF.1710        : MQF__300     , AT = 63.92925          , SLOT_ID = 5529031;                                   !MP= 63.975520
 L4T.VVGS.1751       : VVGAC        , AT = 64.44925          , SLOT_ID = 5529032;                                   !MP= 64.495520
 L4T.ENDL4T.0000     : L4TOMK       , AT = 70.12612          , SLOT_ID = 5542741;                                   !MP= 70.172390
ENDSEQUENCE;

!++END_SEQUENCE!++

!++STRENGTH!++

/************************************************************************************/
/*                      STRENGTH CONSTANT                                           */
/************************************************************************************/

REAL CONST angle.L4T.MBH.0250   := 0.407243448374;
REAL CONST angle.L4T.MBH.0450   := 0.407243448374;
REAL CONST angle.L4T.MBH.0650   := 0.407243448374;
REAL CONST angle.L4T.MBV.1250   := 0.2526079601;
REAL CONST angle.L4T.MBV.1550   := -0.2526079601;
REAL CONST e1.L4T.MBH.0250      := 0.203621724187;
REAL CONST e1.L4T.MBH.0450      := 0.203621724187;
REAL CONST e1.L4T.MBH.0650      := 0.203621724187;
REAL CONST e1.L4T.MBV.1250      := 0.12630398005;
REAL CONST e1.L4T.MBV.1550      := -0.12630398005;
REAL CONST e2.L4T.MBH.0250      := 0.203621724187;
REAL CONST e2.L4T.MBH.0450      := 0.203621724187;
REAL CONST e2.L4T.MBH.0650      := 0.203621724187;
REAL CONST e2.L4T.MBV.1250      := 0.12630398005;
REAL CONST e2.L4T.MBV.1550      := -0.12630398005;
REAL CONST fint.L4T.MBH.0250    := 0.7;
REAL CONST fint.L4T.MBH.0450    := 0.7;
REAL CONST fint.L4T.MBH.0650    := 0.7;
REAL CONST fint.L4T.MBV.1250    := 0;
REAL CONST fint.L4T.MBV.1550    := 0;
REAL CONST hgap.L4T.MBH.0250    := 0.025;
REAL CONST hgap.L4T.MBH.0450    := 0.025;
REAL CONST hgap.L4T.MBH.0650    := 0.025;
REAL CONST hgap.L4T.MBV.1250    := 0.025;
REAL CONST hgap.L4T.MBV.1550    := 0.025;
REAL CONST l.L4T.MBH.0250       := 0.876041170758117;
REAL CONST l.L4T.MBH.0450       := 0.876041170758117;
REAL CONST l.L4T.MBH.0650       := 0.876041170758117;
REAL CONST l.L4T.MBV.1250       := 0.872317453179938;
REAL CONST l.L4T.MBV.1550       := 0.872317453179938;
REAL CONST tilt.L4T.MBV.1250    := -pi/2;
REAL CONST tilt.L4T.MBV.1550    := -pi/2;

/************************************************************************************/
/*                       STRENGTHS                                                  */
/************************************************************************************/

 L4T.MBH.0250,      L := l.L4T.MBH.0250, ANGLE := angle.L4T.MBH.0250, E1 := e1.L4T.MBH.0250, E2 := e2.L4T.MBH.0250, FINT := fint.L4T.MBH.0250, HGAP := hgap.L4T.MBH.0250;
 L4T.MBH.0450,      L := l.L4T.MBH.0450, ANGLE := angle.L4T.MBH.0450, E1 := e1.L4T.MBH.0450, E2 := e2.L4T.MBH.0450, FINT := fint.L4T.MBH.0450, HGAP := hgap.L4T.MBH.0450;
 L4T.MBH.0650,      L := l.L4T.MBH.0650, ANGLE := angle.L4T.MBH.0650, E1 := e1.L4T.MBH.0650, E2 := e2.L4T.MBH.0650, FINT := fint.L4T.MBH.0650, HGAP := hgap.L4T.MBH.0650;
 L4T.MBV.1250,      L := l.L4T.MBV.1250, ANGLE := angle.L4T.MBV.1250, TILT := tilt.L4T.MBV.1250, E1 := e1.L4T.MBV.1250, E2 := e2.L4T.MBV.1250, FINT := fint.L4T.MBV.1250, HGAP := hgap.L4T.MBV.1250;
 L4T.MBV.1550,      L := l.L4T.MBV.1550, ANGLE := angle.L4T.MBV.1550, TILT := tilt.L4T.MBV.1550, E1 := e1.L4T.MBV.1550, E2 := e2.L4T.MBV.1550, FINT := fint.L4T.MBV.1550, HGAP := hgap.L4T.MBV.1550;

!++END_STRENGTH!++


! The signs of all K1 from Alessandra's input are inverted, so that MADX can simulate a H- beam (negative charge).

 L4T.MQD.0110,  K1 := -L4T.kMQD.0110;
 L4T.MQF.0210,  K1 := -L4T.kMQF.0210;
 L4T.MQD.0310,  K1 := -L4T.kMQD.0310;
 L4T.MQF.0410,  K1 := -L4T.kMQF.0410;
 L4T.MQF.0510,  K1 := -L4T.kMQF.0510;
 L4T.MQD.0610,  K1 := -L4T.kMQD.0610;
 L4T.MQF.0710,  K1 := -L4T.kMQF.0710;
 L4T.MQD.0810,  K1 := -L4T.kMQD.0810;
 L4T.MQF.0910,  K1 := -L4T.kMQF.0910;
 L4T.MQD.1010,  K1 := -L4T.kMQD.1010;
 L4T.MQD.1110,  K1 := -L4T.kMQD.1110;
 L4T.MQF.1210,  K1 := -L4T.kMQF.1210;
 L4T.MQD.1310,  K1 := -L4T.kMQD.1310;
 L4T.MQF.1410,  K1 := -L4T.kMQF.1410;
 L4T.MQD.1510,  K1 := -L4T.kMQD.1510;
 L4T.MQD.1610,  K1 := -L4T.kMQD.1610;
 L4T.MQF.1710,  K1 := -L4T.kMQF.1710;

/*****************************************************************************
 * set initial twiss parameters
 *****************************************************************************/
! input conditions from Alessandra Lombardi/Giulia Bellodi

L4T.BETX0 =  5.5270; ![m/rad]
L4T.ALFX0 =  1.7357;
L4T.MUX0  =  0.0;  !phase function [rad]
L4T.BETY0 =  11.0314; ![m/rad]
L4T.ALFY0 =  -3.0024;
L4T.MUY0  =  0.0;  !phase function [rad]
L4T.X0    =  0.0;
L4T.PX0   =  0.0;
L4T.Y0    =  0.0;
L4T.PY0   =  0.0;
L4T.T0    =  0.0; !longitudinal displacement
L4T.PT0   =  0.0; !longitudinal transverse momentum
L4T.DX0   =  0.0; !Dispersion of x
L4T.DPX0  =  0.0; !Dispersion of px
L4T.DY0   =  0.0; !Dispersion of y
L4T.DPY0  =  0.0; !Dispersion of py


/*****************************************************************************
 * store initial parameters in memory block
 *****************************************************************************/ 
L4T.INITBETA0:  BETA0,
        BETX := L4T.BETX0,
        ALFX := L4T.ALFX0,
        MUX  := L4T.MUX0,
        BETY := L4T.BETY0,
        ALFY := L4T.ALFY0,
        MUY  := L4T.MUY0,
        X    := L4T.X0,
        PX   := L4T.PX0,
        Y    := L4T.Y0,
        PY   := L4T.PY0,
        T    := L4T.T0,
        PT   := L4T.PT0,
        DX   := L4T.DX0,
        DPX  := L4T.DPX0,
        DY   := L4T.DY0,
        DPY  := L4T.DPY0;


return;
