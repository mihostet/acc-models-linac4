!++HEAD!++
/*************************************************************************************
*
* LT version STUDY in MAD X SEQUENCE format
* Generated the 18-MAY-2020 13:17:05 from LHCLAYOUT@EDMSDB Database
*
*************************************************************************************/
!++END_HEAD!+++

!++ELEMENTS!++

/************************************************************************************/
/*                       TYPES DEFINITION                                           */
/************************************************************************************/

//---------------------- HKICKER        ---------------------------------------------
DHZ__09B  : HKICKER     , L := 0;         ! DHZ Horizontal Dipole Corrector Magnet type 9 b
//---------------------- MARKER         ---------------------------------------------
BEC       : MARKER      , L := 0;         ! Carbon ring collimator
LTOMK     : MARKER      , L := 0;         ! LT transfer line Markers for the MAD File
VPI__001  : MARKER      , L := 0;         ! Vacuum Ion Pump (in LT and LTB lines)
VVS       : MARKER      , L := 0;         ! Vacuum Sector Valve (PS Complex)
//---------------------- MONITOR        ---------------------------------------------
BCTFT     : MONITOR     , L := 0;         ! Fast Beam Current Transformer, type T
BPUSE011  : MONITOR     , L := 0;         ! Beam Position Monitor, Stripline, variant E.
//---------------------- QUADRUPOLE     ---------------------------------------------
MQNAIFAP  : QUADRUPOLE  , L := 0.25500;   ! Quadrupole magnet, type Q11 or linac VII
MQNBSNAP  : QUADRUPOLE  , L := 0.45200;   ! Quadrupole magnet, booster injection
//---------------------- RBEND          ---------------------------------------------
MBHDAHWP  : RBEND       , L := 0.40544;   ! Bending magnet, type BHZ, 0.4m
//---------------------- SBEND          ---------------------------------------------
MBHEACWP  : SBEND       , L := 1.05274;   ! Bending magnet, type BHZ, 0.9m, Linac2
//---------------------- VKICKER        ---------------------------------------------
DVT__09B  : VKICKER     , L := 0;         ! DVT Vertical Dipole Corrector Magnet type 9 b

!return;
!++END_ELEMENTS!++


!++SEQUENCE!++

/************************************************************************************/
/*                       LT SEQUENCE                                                */
/************************************************************************************/

LT : SEQUENCE, REFER = centre,     L = 29.928517649;
 LT.BEGLT       : LTOMK     , AT = 0            , SLOT_ID = 5409262;
 LT.BHZ20       : MBHEACWP  , AT = .756369      , SLOT_ID = 3501157;
 LT.BEC3        : BEC       , AT = 6.108474     , SLOT_ID = 5332542;
 LT.QFO50       : MQNAIFAP  , AT = 7.378474     , SLOT_ID = 3501159;
 LT.VPI14       : VPI__001  , AT = 8.328474     , SLOT_ID = 3777280;
 LT.BEC4        : BEC       , AT = 8.748474     , SLOT_ID = 5332543;
 LT.BCT30       : BCTFT     , AT = 9.075974     , SLOT_ID = 3501160; ! Real position needs to be checked
 LT.DHZ40       : DHZ__09B  , AT = 9.628474     , SLOT_ID = 3501161, ASSEMBLY_ID = 3501106;
 LT.DVT40       : DVT__09B  , AT = 9.628474     , SLOT_ID = 3501162, ASSEMBLY_ID = 3501106;
 LT.BPM40       : BPUSE011  , AT = 10.788474    , SLOT_ID = 3501165;
 LT.QDE55       : MQNAIFAP  , AT = 11.578474    , SLOT_ID = 3501166;
 LT.VVS20       : VVS       , AT = 11.940474    , SLOT_ID = 3501167;
 LT.QFO60       : MQNAIFAP  , AT = 16.448474    , SLOT_ID = 3501168;
 LT.QDE65       : MQNAIFAP  , AT = 18.356474    , SLOT_ID = 3501169;
 !LT.BHZ25       : MBHDAHWP  , AT = 18.956474    , SLOT_ID = 3501170; ! Out of service
 LT.VPI25       : VPI__001  , AT = 20.253474    , SLOT_ID = 3777281;
 LT.VPI26       : VPI__001  , AT = 22.849474    , SLOT_ID = 3777282;
 LT.QDE70       : MQNBSNAP  , AT = 23.488474    , SLOT_ID = 3501171;
 LT.QFO75       : MQNAIFAP  , AT = 26.878474    , SLOT_ID = 3501172;
 LT.DHZ50       : DHZ__09B  , AT = 27.848474    , SLOT_ID = 3501173, ASSEMBLY_ID = 3501107;
 LT.DVT50       : DVT__09B  , AT = 27.848474    , SLOT_ID = 3501174, ASSEMBLY_ID = 3501107;
 LT.BCT40       : BCTFT     , AT = 28.804824    , SLOT_ID = 3501175; ! Real position needs to be checked
 LT.BPM50       : BPUSE011  , AT = 29.252074    , SLOT_ID = 3501176; ! Real position needs to be checked
 LT.ENDLT       : LTOMK     , AT = 29.928518    , SLOT_ID = 5409266;
ENDSEQUENCE;

!++END_SEQUENCE!++

!++STRENGTH!++

/************************************************************************************/
/*                      STRENGTH CONSTANT                                           */
/************************************************************************************/

REAL CONST angle.LT.BHZ20       := -0.420475953;
REAL CONST e1.LT.BHZ20          := -0.2102379765;
REAL CONST e2.LT.BHZ20          := -0.2102379765;
REAL CONST fint.LT.BHZ20        := 0;
REAL CONST hgap.LT.BHZ20        := 0.05;
REAL CONST l.LT.BHZ20           := 1.05273805058656;

/************************************************************************************/
/*                       STRENGTHS                                                  */
/************************************************************************************/

 LT.BHZ20,          L := l.LT.BHZ20, ANGLE := angle.LT.BHZ20, E1 := e1.LT.BHZ20, E2 := e2.LT.BHZ20, FINT := fint.LT.BHZ20, HGAP := hgap.LT.BHZ20;


 ! The signs of all K1 from ABP input are inverted, so that MADX can simulate a H- beam (negative charge).

 LT.QFO50,    K1 := -LT.kQFO50;
 LT.QDE55,    K1 := -LT.kQDE55;
 LT.QFO60,    K1 := -LT.kQFO60;
 LT.QDE65,    K1 := -LT.kQDE65;
 LT.QDE70,    K1 := -LT.kQDE70;
 LT.QFO75,    K1 := -LT.kQFO75;


!++END_STRENGTH!++
return;
