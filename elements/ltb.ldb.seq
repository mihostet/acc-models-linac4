!extracted on 2020.05.18 from https://lhclayout-old.web.cern.ch/madsequence.aspx?machine=LTB
!++HEAD!++
/*************************************************************************************
*
* LTB version STUDY in MAD X SEQUENCE format
* Generated the 18-MAY-2020 13:04:58 from LHCLAYOUT@EDMSDB Database
*
*************************************************************************************/
!++END_HEAD!+++

!++ELEMENTS!++

/************************************************************************************/
/*                       TYPES DEFINITION                                           */
/************************************************************************************/

//---------------------- HKICKER        ---------------------------------------------
DHZ__09B  : HKICKER     , L := 0;         ! DHZ Horizontal Dipole Corrector Magnet type 9 b
//---------------------- INSTRUMENT     ---------------------------------------------
BCTFT002  : INSTRUMENT  , L := 0;         ! Fast Beam Current Transformer, type T
//---------------------- MARKER         ---------------------------------------------
LTBOMK    : MARKER      , L := 0;         ! LTB transfer line Markers for the MAD File
SLH       : MARKER      , L := 0;         ! Horizontal slit
SLV       : MARKER      , L := 0;         ! Vertical slit
VPI__001  : MARKER      , L := 0;         ! Vacuum Ion Pump (in LT and LTB lines)
VVS       : MARKER      , L := 0;         ! Vacuum Sector Valve (PS Complex)
//---------------------- MONITOR        ---------------------------------------------
BPUSE002  : MONITOR     , L := 0;         ! Beam Position Monitor, Stripline, variant E.
BPUSE011  : MONITOR     , L := 0;         ! Beam Position Monitor, Stripline, variant E.
MSFH      : MONITOR     , L := 0;         ! SEM fil horizontal
MSFHV     : MONITOR     , L := 0;         ! Assembly SEM fil horizontal/Vertical
MSFV      : MONITOR     , L := 0;         ! SEM fil vertical
//---------------------- QUADRUPOLE     ---------------------------------------------
MQNAIFAP  : QUADRUPOLE  , L := 0.25500;   ! Quadrupole magnet, type Q11 or linac VII
MQNBSNAP  : QUADRUPOLE  , L := 0.45200;   ! Quadrupole magnet, booster injection
//---------------------- SBEND          ---------------------------------------------
MBHEBCWP  : SBEND       , L := 1.05145;   ! Bending magnet, type IBH1
//---------------------- VKICKER        ---------------------------------------------
DVT__09B  : VKICKER     , L := 0;         ! DVT Vertical Dipole Corrector Magnet type 9 b

!return;
!++END_ELEMENTS!++


!++SEQUENCE!++

/************************************************************************************/
/*                       LTB SEQUENCE                                               */
/************************************************************************************/

LTB : SEQUENCE, REFER = centre,    L = 29.16156022;
 LTB.BEGLTB     : LTBOMK    , AT = 0            , SLOT_ID = 5409267;
 LT.BHZ30       : MBHEBCWP  , AT = .8134235     , SLOT_ID = 3501177;
 LTB.DHZ10      : DHZ__09B  , AT = 2.052368     , SLOT_ID = 3501178, ASSEMBLY_ID = 3501108;
 LTB.DVT10      : DVT__09B  , AT = 2.052368     , SLOT_ID = 3501179, ASSEMBLY_ID = 3501108;
 LTB.VVS10      : VVS       , AT = 2.946868     , SLOT_ID = 3501181;
 LTB.VPI11      : VPI__001  , AT = 3.449868     , SLOT_ID = 3777283;
 LTB.QNO10      : MQNAIFAP  , AT = 5.106868     , SLOT_ID = 3501182;
 LTB.QNO20      : MQNAIFAP  , AT = 6.106868     , SLOT_ID = 3501183;
 LTB.MSFH30     : MSFH      , AT = 6.544868     , SLOT_ID = 3501184, ASSEMBLY_ID = 5332744;
 LTB.MSFHV30    : MSFHV     , AT = 6.544868     , SLOT_ID = 5332744;
 LTB.MSFV30     : MSFV      , AT = 6.544868     , SLOT_ID = 4750734, ASSEMBLY_ID = 5332744;
 LTB.BPM10      : BPUSE002  , AT = 7.252368     , SLOT_ID = 3501185;
 LTB.DHZ20      : DHZ__09B  , AT = 7.956868     , SLOT_ID = 3501187, ASSEMBLY_ID = 3501109;
 LTB.DVT20      : DVT__09B  , AT = 7.956868     , SLOT_ID = 3501188, ASSEMBLY_ID = 3501109;
 LTB.BCT50      : BCTFT002  , AT = 10.958868    , SLOT_ID = 3501189; ! Real position needs to be checked
 LTB.BPM20      : BPUSE011  , AT = 14.456868    , SLOT_ID = 3501191;
 LTB.DHZ30      : DHZ__09B  , AT = 14.956868    , SLOT_ID = 3501192, ASSEMBLY_ID = 3501110;
 LTB.DVT30      : DVT__09B  , AT = 14.956868    , SLOT_ID = 3501193, ASSEMBLY_ID = 3501110;
 LTB.QNO30      : MQNBSNAP  , AT = 15.856868    , SLOT_ID = 3501194;
 LTB.QNO40      : MQNBSNAP  , AT = 16.856868    , SLOT_ID = 3501195;
 !LTB.SLH10      : SLH       , AT = 21.229868    , SLOT_ID = 3501196; ! Out of service.The exact position is not known
 !LTB.SLV10      : SLV       , AT = 22.047868    , SLOT_ID = 3501197; ! Out of service.The exact position is not known
 LTB.VPI12      : VPI__001  , AT = 22.278868    , SLOT_ID = 3777284;
 LTB.VPI13      : VPI__001  , AT = 24.794868    , SLOT_ID = 3777287;
 LTB.MSFH40     : MSFH      , AT = 25.324868    , SLOT_ID = 3501199, ASSEMBLY_ID = 5332749;
 LTB.MSFHV40    : MSFHV     , AT = 25.324868    , SLOT_ID = 5332749;
 LTB.MSFV40     : MSFV      , AT = 25.324868    , SLOT_ID = 4750735, ASSEMBLY_ID = 5332749;
 LTB.BCT60      : BCTFT002  , AT = 25.706868    , SLOT_ID = 3501200; ! Real position needs to be checked
 LTB.QNO50      : MQNBSNAP  , AT = 26.406868    , SLOT_ID = 3501201;
 LTB.QNO60      : MQNBSNAP  , AT = 27.706868    , SLOT_ID = 3501202;
 LTB.DHZ40      : DHZ__09B  , AT = 28.356868    , SLOT_ID = 3501203, ASSEMBLY_ID = 3501111;
 LTB.DVT40      : DVT__09B  , AT = 28.356868    , SLOT_ID = 3501204, ASSEMBLY_ID = 3501111;
 LTB.BPM30      : BPUSE011  , AT = 28.860868    , SLOT_ID = 3501205;
 LTB.ENDLTB     : LTBOMK    , AT = 29.16156     , SLOT_ID = 5409270;
ENDSEQUENCE;

!++END_SEQUENCE!++

!++STRENGTH!++

/************************************************************************************/
/*                      STRENGTH CONSTANT                                           */
/************************************************************************************/

REAL CONST angle.LT.BHZ30       := -0.383950409938051;
REAL CONST e1.LT.BHZ30          := -0.191975204969025;
REAL CONST e2.LT.BHZ30          := -0.191975204969025;
REAL CONST fint.LT.BHZ30        := 1;
REAL CONST hgap.LT.BHZ30        := 0.052;
REAL CONST l.LT.BHZ30           := 1.05144652841087;

/************************************************************************************/
/*                       STRENGTHS                                                  */
/************************************************************************************/

 LT.BHZ30,          L := l.LT.BHZ30, ANGLE := angle.LT.BHZ30, E1 := e1.LT.BHZ30, E2 := e2.LT.BHZ30, FINT := fint.LT.BHZ30, HGAP := hgap.LT.BHZ30;



!-- (IE) depending on what the K values here represent, they may or may not divided by the beam rigidity
!K1 = Gradient/Brho
!Brho =0.57112(GeV/c)*3.3356=1.905027872 Tm

 ! The signs of all K1 from ABP input are inverted, so that MADX can simulate a H- beam (negative charge).
 LTB.QNO10, K1 := -LTB.kQNO10;
 LTB.QNO20, K1 := -LTB.kQNO20;
 LTB.QNO30, K1 := -LTB.kQNO30;
 LTB.QNO40, K1 := -LTB.kQNO40;
 LTB.QNO50, K1 := -LTB.kQNO50;
 LTB.QNO60, K1 := -LTB.kQNO60;


!++END_STRENGTH!++
return;
