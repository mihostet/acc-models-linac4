/*****************************************************
 *
 * MADX file for the L4T-LT-LTB-LBE line from LINAC4 to the LBE
 *
 *
 * Execute with:  >madx < l4t_lt_ltb_lbe.madx
 * This file is for H- at Ekin=160MeV
 *****************************************************/

/*****************************************************************************
 * TITLE
 *****************************************************************************/
hor_emit = 1; ! 0 = vertical meas. optics /  1 = horiz. meas. optics

value, hor_emit;

if (hor_emit == 1) {
   title, 'L4T-LT-LTB-LBE postLS2 horizontal emittance optics. H- E_kin=160 MeV';
} else {
   title, 'L4T-LT-LTB-LBE postLS2 vertical   emittance optics. H- E_kin=160 MeV';
}

 option, -echo;
!option, RBARC=FALSE;

/*****************************************************************************
 * L4T-LT-LTB-LBE line
 * NB! The order of file loading matters:
 *     Use like this :   .ele > .seq > _ele.str > .str > .dbx   
 *     The reason is a >feature< of MADX
 *
 *****************************************************************************/

 call, file = '../elements/l4t.ele';
 call, file = '../elements/lt.ele';
 call, file = '../elements/ltb.ele';
 call, file = '../elements/lbe.ele';

 call, file = '../sequence/l4t.seq';
 call, file = '../sequence/lt.seq';
 call, file = '../sequence/ltb.seq';
 call, file = '../sequence/lbe.seq';

! here it is where the bendings are defined
 call, file = '../strength/l4t_ele.str';
 call, file = '../strength/lt_ele.str';
 call, file = '../strength/ltb_ele.str';
 
! here it is where the quads are defined
 call, file = '../strength/l4t.str';
 call, file = '../strength/lt.str';
 call, file = '../strength/ltb.str';

! debuncher file
 call, file = '../strength/debuncherPlus1000kV.str';

if (hor_emit == 1) {
 call, file = '../strength/lbe_hor.str';
} else {
 call, file = '../strength/lbe_ver.str';
}


 !call, file = '../aperture/l4t.dbx';
 !call, file = '../aperture/lt.dbx';
 !call, file = '../apperture/ltb.dbx';
 !call, file = '../apperture/bi.dbx';

/*****************************************************************************
 * build up the geometry of the beam lines
 *****************************************************************************/

len_L4T = 70.12612;
len_LT  = 29.928517649;
len_LTB = 29.16156022; 
len_LBE = 16.05533389;

! for survey the L4T and the LT-LTB-LBE are in 2 different geodic worlds...
l4tltltblbe: sequence, refer=ENTRY,  l = len_L4T+len_LT+len_LTB+len_LBE ;
  l4t                         , at = 0;
  lt                          , at = len_L4T;
  ltb                         , at = len_L4T + len_LT ;
  lbe                         , at = len_L4T + len_LT + len_LTB ;
endsequence;
save, sequence=l4tltltblbe, file=./kr_sav.seq;


/*****************************************************************************
 * set initial twiss parameters
 *****************************************************************************/
call, file = '../inp/l4t.inp';

/*****************************************************************************
 * store initial parameters in memory block
 *****************************************************************************/
INITBETA0: BETA0,
  BETX=BETX0,
  ALFX=ALFX0,
  MUX=MUX0,
  BETY=BETY0,
  ALFY=ALFY0,
  MUY=MUY0,
  X=X0,
  PX=PX0,
  Y=Y0,
  PY=PY0,
  T=T0,
  PT=PT0,
  DX=DX0,
  DPX=DPX0,
  DY=DY0,
  DPY=DPY0;

/*******************************************************************************
 * Beam
 * NB! beam->ex == (beam->exn)/(beam->gamma*beam->beta*4)
 *******************************************************************************/
 BEAM, PARTICLE=Hminus, MASS=.93929, CHARGE=-1, PC=0.57083;! ENERGY=1.09929;
!show, beam;


maketwiss: macro=
 {
     ptc_create_universe;
     ptc_create_layout,model=2,method=6,nst=5,exact,time=true;
     ptc_setswitch, debuglevel=1, time=true, fringe=true, nocavity=false;
     ptc_twiss, table=twiss, BETA0=initbeta0, betz=0.1, icase=6, no=3,file="twiss";
     ptc_end;

 };


!use, sequence=l4t;
use, sequence=l4tltltblbe;
exec, maketwiss;

select,flag=twiss,clear;
select,flag=twiss,column=name,s,x,y,beta11,beta22,disp1,disp3;

write, table=twiss, file="kickcheck.twiss.ptc.tfs";

plot, haxis=s, vaxis=x,y, colour=100;
plot, haxis=s, vaxis=beta11,beta22, colour=100;


	            
option,-echo,-info;
! plane H = 1
! else  plane V
kickresponse_L4T(thekicker, thekick, plane) : macro={                                 

                                 

                                 kick_rad = thekick*1e-6;
                                 ! value, kick_rad;

                                 if (plane == 1) {
                                    thekicker->HKICK := kick_rad;
                                 } else {
                                    thekicker->VKICK := kick_rad;
                                 }                                 
                                 use, sequence=l4tltltblbe;
                                 exec, maketwiss;
                                 
                                 set,  format="-18s";
                                 set,  format="10.5f";

                                 select,flag=twiss,column=name,s,x,y;
                                 !twiss,BETA0=INITBETA0,save,centre,file=twiss.out;

                                 if (thekick > 0) { 
                                   if (plane == 1) {
                                      exec, maketwiss;
                                      write,table=twiss,file="../out/kr_ltltblbe_thekicker_H_plus_deb.out";
                                   } else {
                                      exec, maketwiss;
                                      write,table=twiss,file="../out/kr_ltltblbe_thekicker_V_plus_deb.out";
                                   }
                                 }

                                 if (thekick < 0) { 
                                   if (plane == 1) {
                                      exec, maketwiss;
                                      write,table=twiss,file="../out/kr_ltltblbe_thekicker_H_minus_deb.out";
                                   } else {
                                      exec, maketwiss;
                                      write,table=twiss,file="../out/kr_ltltblbe_thekicker_V_minus_deb.out";
                                   }
                                 }


                                 ! make sure the kicker is reset to zero                  
                                 thekicker->HKICK := 0.0000;
                                 thekicker->VKICK := 0.0000;
                                }


kickresponse_LTLTB(thekicker, thekick) : macro={                                 
                                 
                                 kick_rad = thekick*1e-6;
                                 ! value, kick_rad;
                                 thekicker->KICK := kick_rad;

                                 use, sequence=l4tltltblbe;
                                 
                                 set,  format="-18s";
                                 set,  format="10.5f";


                                 select,flag=twiss,column=name,s,x,y;
                                 twiss,BETA0=INITBETA0,save,centre,file=twiss.out;

                                 if (thekick > 0) { 
                                   twiss,BETA0=INITBETA0,save,centre,file="../out/kr_ltltblbe_thekicker_plus_deb.out";
                                                   }
                                 if (thekick < 0) { 
                                   twiss,BETA0=INITBETA0,save,centre,file="../out/kr_ltltblbe_thekicker_minus_deb.out";
                                                   }


                                 ! make sure the kicker is reset to zero                  
                                 thekicker->KICK := 0.0000;
                                }


/*******************************************************************************
 * kick response measurements
 *******************************************************************************/
 
! kick_pos =  500; ! urad
! kick_neg = -500; ! urad

kick_pos =  150/3; ! urad
kick_neg = -150/3; ! urad

/*******************************************************************************
 * L4T line
 *******************************************************************************/

!exec, kickresponse_L4T(L4T.MCHV.0105, kick_pos, 1); 
!exec, kickresponse_L4T(L4T.MCHV.0105, kick_neg, 1); 

!exec, kickresponse_L4T(L4T.MCHV.0735, kick_pos, 0); 
!exec, kickresponse_L4T(L4T.MCHV.0735, kick_neg, 0); 

exec, kickresponse_L4T(L4T.MCHV.0315, kick_pos, 0); 
exec, kickresponse_L4T(L4T.MCHV.0315, kick_neg, 0); 

!exec, kickresponse_LTLTB(LT.DVT40, kick_pos); 
!exec, kickresponse_LTLTB(LT.DVT40, kick_neg); 










///*******************************************************************************
// * Use
// *******************************************************************************/
//
// use, sequence=l4tltltblbe;
//
//
///*******************************************************************************
// * twiss
// *******************************************************************************/
//
// maketwiss: macro=
// {
//     ptc_create_universe;
//     ptc_create_layout,model=2,method=6,nst=5,exact,time=false;
//     ptc_twiss,table=ptc_twiss,BETA0=INITBETA0,icase=5,no=1; ! ,file="twiss";
//     ptc_end;
//
// };
//
// exec, maketwiss;
//
//!write,table=ptc_twiss;
//!value,table(ptc_twiss,LTLTBBI$START,alfa11);
//!value,table(ptc_twiss,LT.DHZ10,beta11);
//
//
///***************************************************
// * Write ptc_twiss table. NB! Values at end of elements
// ***************************************************/
//set,  format="-18s";
//set,  format="10.5f";
//
//
// select,flag=ptc_twiss,clear;
// ! the one to be used for YASP output
// !select,flag=ptc_twiss, column = name,angle,k1L,k2L,k3L,beta11,beta22,disp1,disp3,x,y,alfa11,alfa22,mu1,mu2,disp2,disp4,px,py;
//select,flag=ptc_twiss, column = name,s,angle,k1L,k2L,k3L,beta11,beta22,disp1,disp3,x,y,alfa11,alfa22,mu1,mu2,disp2,disp4,px,py;
// exec, maketwiss;
//if (hor_emit == 1) {
// write,table=ptc_twiss,file="../out/l4tltltblbe_hor.out";
//} else {
// write,table=ptc_twiss,file="../out/l4tltltblbe_ver.out";
//}
//
//
//
//
///*******************************************************************************
// * Plot l4tltltblbe_xxx
// *******************************************************************************/
//
//option, -info;
//option, -echo;
//
//resplot;
//setplot, post=2;
//
//if (hor_emit == 1) {
//   plot, title='lbe hor.'  , table=ptc_twiss
//                           , haxis=s
//                           , vaxis1=beta11,beta22
//                           , style:=100,symbol:=4,colour=100
//                           , file = "../out/l4tltltblbe_hor";
//
//   plot, title='lbe hor.'  , table=ptc_twiss
//                           , haxis=s
//                           , vaxis1=beta11,beta22
//                           , vaxis2=disp1
//                           , range=#S/#e
//                           , style:=100, symbol:=4, colour=100;
//
//   plot, title='lbe hor.'  , table=ptc_twiss
//                           , haxis=s
//                           , vaxis1=disp1,disp3
//                           , range=#S/#e
//                           , style:=100, symbol:=4, colour=100;
//} else {
//   plot, title='lbe ver.'  , table=ptc_twiss
//                           , haxis=s
//                           , vaxis1=beta11,beta22
//                           , style:=100,symbol:=4,colour=100
//                           , file = "../out/l4tltltblbe_ver";
//
//   plot, title='lbe ver.'  , table=ptc_twiss
//                           , haxis=s
//                           , vaxis1=beta11,beta22
//                           , vaxis2=disp1
//                           , range=#S/#e
//                           , style:=100, symbol:=4, colour=100;
//
//   plot, title='lbe ver.'  , table=ptc_twiss
//                           , haxis=s
//                           , vaxis1=disp1,disp3
//                           , range=#S/#e
//                           , style:=100, symbol:=4, colour=100;
//}
//
/*******************************************************************************
 * Clean up 
 *******************************************************************************/
system, "rm internal_mag_pot.txt";


stop;
