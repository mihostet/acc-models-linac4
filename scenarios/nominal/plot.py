from matplotlib.pyplot import *
from pyoptics import *


#tw = optics.open("twiss.tfs")
#tw.ndx()

tfsfile = "L4TtoR3.twiss.ptc.tfs";
dirnames = ["debuncher_0kV",
            "debuncher_minus750kV",
            "debuncher_plus650kV"]
            
p0 = optics.open(dirnames[0] + "/"+ tfsfile)
p1 = optics.open(dirnames[1] + "/"+ tfsfile)
p2 = optics.open(dirnames[2] + "/"+ tfsfile)


p0.betx_0kV    = p0.beta11
p0.betx_m750kV = p1.beta11
p0.betx_p650kV = p2.beta11

plot = p0.plot('betx_0kV betx_m750kV betx_p650kV')

p0.bety_0kV    = p0.beta22
p0.bety_m750kV = p1.beta22
p0.bety_p650kV = p2.beta22

plot = p0.plot('bety_0kV bety_m750kV bety_p650kV')

p0.DX_0kV    = p0.disp1
p0.DX_m750kV = p1.disp1
p0.DX_p650kV = p2.disp1

plot = p0.plot('DX_0kV DX_m750kV DX_p650kV')

p0.DY_0kV    = p0.disp3
p0.DY_m750kV = p1.disp3
p0.DY_p650kV = p2.disp3

plot = p0.plot('DY_0kV DY_m750kV DY_p650kV')

print("param\t ","    0kV","\t ","+650 kV","\t ","-750 kV\n")
print("betaX\t ",p0.beta11[-1],
           "\t ",p1.beta11[-1],
           "\t ",p2.beta11[-1])
print("  rel\t ","%0.4f"%((p0.beta11[-1]-p0.beta11[-1])/p0.beta11[-1]),
           "\t ","%0.4f"%((p1.beta11[-1]-p0.beta11[-1])/p0.beta11[-1]),
           "\t ","%0.4f"%((p2.beta11[-1]-p0.beta11[-1])/p0.beta11[-1]), "\n" )

print("betaY\t ",p0.beta22[-1],
           "\t ",p1.beta22[-1],
           "\t ",p2.beta22[-1])
print("  rel\t ","%0.4f"%((p0.beta22[-1]-p0.beta22[-1])/p0.beta22[-1]),
           "\t ","%0.4f"%((p1.beta22[-1]-p0.beta22[-1])/p0.beta22[-1]),
           "\t ","%0.4f"%((p2.beta22[-1]-p0.beta22[-1])/p0.beta22[-1]), "\n" )


print("dispX\t ",p0.disp1[-1],
           "\t ",p1.disp1[-1],
           "\t ",p2.disp1[-1])
print("  rel\t ","%0.4f"%((p0.disp1[-1]-p0.disp1[-1])/p0.disp1[-1]),
           "\t ","%0.4f"%((p1.disp1[-1]-p0.disp1[-1])/p0.disp1[-1]),
           "\t ","%0.4f"%((p2.disp1[-1]-p0.disp1[-1])/p0.disp1[-1]), "\n" )


print("dispY\t ",p0.disp3[-1],
           "\t ",p1.disp3[-1],
           "\t ",p2.disp3[-1])
print("  rel\t ","%0.4f"%((p0.disp3[-1]-p0.disp3[-1])/p0.disp3[-1]),
           "\t ","%0.4f"%((p1.disp3[-1]-p0.disp3[-1])/p0.disp3[-1]),
           "\t ","%0.4f"%((p2.disp3[-1]-p0.disp3[-1])/p0.disp3[-1]), "\n" )


ion()
show()


g = input("Hit any key : ") 

