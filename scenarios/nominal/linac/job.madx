/******************************************************************************************
 *
 * MAD-X input script for the 2019 LBE Run.
 *
 * 18/05/2020 - Piotr Skowronski
 ******************************************************************************************/

!option, -echo;
!option, debug;

conf.inuse.ring = 3;  
conf.dirlevel = 3;
conf.inuse.linac = 1;
!conf.inuse.linac.wcav = 1; not operational

call, file="linac.str";

call, file="../../../elements/all.seqx";

call, file="../../../madx/macros_ptc.madx";


set,  format="-18s";
set,  format="10.5f";

use sequence=linac4l4z;                                                                                                                  

!show, LINAC.INITBETA0;

twiss, rmatrix, 
beta0 = LINAC.INITBETA0, 
       file = "linac.twiss.tfs"; 
 
resplot;
setplot, post=2;

plot, haxis=s, vaxis1=betx,bety,vaxis2=dx,dy,colour=100,interpolate=true;   
plot, haxis=s, vaxis=betx,bety,colour=100,interpolate=true, vmax=100;   
plot, haxis=s, vaxis=x,y,colour=100,interpolate=false;  
plot, haxis=s, vaxis1=re56, colour=100;


exec, m_ptc_twiss_colsel;

ptc_create_universe;
   ptc_create_layout,model=2,method=6,nst=5,exact,time=true;
   ptc_setswitch, debuglevel=1, time=true, fringe=true, nocavity=false;
   ptc_twiss, table=ptc_twiss, rmatrix, BETA0=linac.initbeta0, betz=0.1, icase=6, no=3;
ptc_end;

system, "rm internal_mag_pot.txt";

write, table=ptc_twiss, file="linac.twiss.ptc.tfs";
exec, m_ptc_twiss_plotEps();



stop;

! Test that 1mm orbit offset does not make crazy trajectory
LINAC.X0 = 1e-3;
LINAC.Y0 = 1e-3;
twiss, rmatrix, beta0 = LINAC.INITBETA0; 
plot, haxis=s, vaxis=x,y,colour=100,interpolate=false;  


BEAM, MASS = 0.939293, 
      CHARGE = -1, 
      ENERGY = 0.942293 + 0.157, 
      EXN = 0.0000004, 
      EYN = 0.0000004, 
      SIGT = 1, 
      SIGE = 1;
use sequence=linac4l4z;                                                                                                                  
twiss, rmatrix, beta0 = LINAC.INITBETA0; 
plot, haxis=s, vaxis=x,y,colour=100,interpolate=false;  


stop;
