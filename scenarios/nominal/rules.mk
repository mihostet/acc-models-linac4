madx:
	/afs/cern.ch/user/m/mad/bin/madx job.madx > job.out 2>&1

clean:
	rm -f job.out madx[0-9][0-9].eps L4TtoR[1-4].twiss.ptc.tfs L4TtoLBE.twiss.ptc.tfs
