/*****************************************************
 *
 * MADX file for the post-LS2 BIx lines
 *   Generates the files for teh survey database (BI3 line)
 *
 * Execute with:  >madx < bi_survey.madx
 * This file is for H- at Ekin=160MeV
 *****************************************************/

/*****************************************************************************
 * TITLE
 *****************************************************************************/
 title, 'BIx 2019 optics. H- E_kin=160 MeV';

 option, echo;
 !option, RBARC=FALSE;

 ldbfiles = 0; ! 1= use the new files in layoutDB directory
 dosurvey = 1; ! 1= execute the survey part
 dotwiss  = 0; ! 1=execute the twiss part
 doplot   = 0; ! 1=do the plot part

 value, ldbfiles;
 value, dosurvey;
 value, dotwiss;
 value, doplot;

/*****************************************************************************
 * BI
 * NB! The order of the .ele .str and .seq files matter.
 *****************************************************************************/
 if (ldbfiles == 1) {
  print text='>>> Running with LDB option.';
  call, file = '../layoutDB/bi_ldb_edit.ele';
  call, file = '../layoutDB/bi_ldb_edit.seq';
 } else {
  print text='>>> Running standard option.';
  call, file = '../elements/bi.ele';
  call, file = '../sequence/bi.seq';
 }
 call, file = '../strength/bi.str';
 call, file = '../aperture/bi.dbx';

/*****************************************************************************
 * set initial twiss parameters
 *****************************************************************************/
 call, file = '../beam/bi.inp';

/*****************************************************************************
 * store initial parameters in memory block
 *****************************************************************************/
 INITBETA0: BETA0,
  BETX  = BETX0,
  ALFX  = ALFX0,
  MUX   = MUX0,
  BETY  = BETY0,
  ALFY  = ALFY0,
  MUY   = MUY0,
  X     = X0,
  PX    = PX0,
  Y     = Y0,
  PY    = PY0,
  T     = T0,
  PT    = PT0,
  DX    = DX0,
  DPX   = DPX0,
  DY    = DY0,
  DPY   = DPY0;

/*******************************************************************************
 * Beam
 * NB! beam->ex == (beam->exn)/(beam->gamma*beam->beta*4)
 *******************************************************************************/
 BEAM, PARTICLE=Hminus, MASS=.93929, CHARGE=-1, PC=0.57083;! ENERGY=1.09929;
!show, beam;

  /*******************************************************************************
   * survey
   *******************************************************************************/
 if (dosurvey == 1) {
     ! -- common start point for all lines
     XS0=-1901.50797; YS0=  2433.66000; ZS0 = 2065.75191; THETAS0 = -5.828898382; ! updated start 36cm upstream to the flange of BHZ40.
     PHIS0   = 0.0;
     PSIS0   = 0.0;

     set,  format="-18s";
     set,  format="15.9f";
     ! set, format="30.20f";
     select, flag=survey,clear;
     select, flag=survey, column=NAME,S,L,ANGLE,X,Y,Z,THETA,PHI,PSI,GLOBALTILT,SLOT_ID, ASSEMBLY_ID;

     surbi: macro={
          if (ldbfiles == 1){
            use, sequence=BI1;
            title, "BI1 BEAM LINE - LS2 CONFIGURATION";
            survey, x0=XS0, y0=YS0, z0=ZS0, theta0=THETAS0, phi0=PHIS0, psi0=PSIS0, file="../test/bi1_ldb_input_for_GEODE.sur";
            save,sequence=BI1,file="../test/bi1_ldb_sav.seq",bare,noexpr;
            dumpsequ, sequence=BI1, level=1;

            use, sequence=BI2;
            title, "BI2 BEAM LINE - LS2 CONFIGURATION";
            survey, x0=XS0, y0=YS0, z0=ZS0, theta0=THETAS0, phi0=PHIS0, psi0=PSIS0, file="../test/bi2_ldb_input_for_GEODE.sur";
            save,sequence=BI1,file="../test/bi2_ldb_sav.seq",bare,noexpr;
            dumpsequ, sequence=BI2, level=1;

            use, sequence=BI3;
            title, "BI3 BEAM LINE - LS2 CONFIGURATION";
            survey, x0=XS0, y0=YS0, z0=ZS0, theta0=THETAS0, phi0=PHIS0, psi0=PSIS0, file="../test/bi3_ldb_input_for_GEODE.sur";
            save,sequence=BI1,file="../test/bi3_ldb_sav.seq",bare,noexpr;
            dumpsequ, sequence=BI3, level=1;

            use, sequence=BI4;
            title, "BI4 BEAM LINE - LS2 CONFIGURATION";
            survey, x0=XS0, y0=YS0, z0=ZS0, theta0=THETAS0, phi0=PHIS0, psi0=PSIS0, file="../test/bi4_ldb_input_for_GEODE.sur";
            save,sequence=BI1,file="../test/bi4_ldb_sav.seq",bare,noexpr;
            dumpsequ, sequence=BI4, level=1;

          } else {
            use, sequence=BI1;
            title, "BI1 BEAM LINE - LS2 CONFIGURATION";
            survey, x0=XS0, y0=YS0, z0=ZS0, theta0=THETAS0, phi0=PHIS0, psi0=PSIS0, file="../survey/bi1_input_for_GEODE.sur";
            save,sequence=BI1,file="../test/bi1_sav.seq",bare,noexpr;
            dumpsequ, sequence=BI1, level=1;

            use, sequence=BI2;
            title, "BI2 BEAM LINE - LS2 CONFIGURATION";
            survey, x0=XS0, y0=YS0, z0=ZS0, theta0=THETAS0, phi0=PHIS0, psi0=PSIS0, file="../survey/bi2_input_for_GEODE.sur";
            save,sequence=BI2,file="../test/bi2_sav.seq",bare,noexpr;
            dumpsequ, sequence=BI2, level=1;

            use, sequence=BI3;
            title, "BI3 BEAM LINE - LS2 CONFIGURATION";
            survey, x0=XS0, y0=YS0, z0=ZS0, theta0=THETAS0, phi0=PHIS0, psi0=PSIS0, file="../survey/bi3_input_for_GEODE.sur";
            save,sequence=BI3,file="../test/bi3_sav.seq",bare,noexpr;
            dumpsequ, sequence=BI3, level=1;

            use, sequence=BI4;
            title, "BI4 BEAM LINE - LS2 CONFIGURATION";
            survey, x0=XS0, y0=YS0, z0=ZS0, theta0=THETAS0, phi0=PHIS0, psi0=PSIS0, file="../survey/bi4_input_for_GEODE.sur";
            save,sequence=BI4,file="../test/bi4_sav.seq",bare,noexpr;
            dumpsequ, sequence=BI4, level=1;

          }
     }
     exec, surbi;
 }

stop;
