/*****************************************************
 *
 * MADX file for the L4Z line to generate Survey Data
 *
 * Execute with:  >madx < l4z.madx
 * This file is for H- at Ekin=160MeV
 *
 *****************************************************/

 title, 'l4z 2019 optics. H- E_kin=160 MeV';

 ldbfiles = 0; ! 1= use the new files in layoutDB directory, the output is then in the test directory

 value, ldbfiles;

/*****************************************************************************
 * l4z
 * NB! The order of the .ele .str and .seq files matter.
 *****************************************************************************/
 if (ldbfiles == 1) {
  print text='>>> Running with LDB option.';
 } else {
  print text='>>> Running standard option.';
  call, file = '../elements/l4t.ele';
  call, file = '../elements/l4z.ele';
  call, file = '../sequence/l4z.seq';
 }

 call, file = '../aperture/l4t.dbx';
 call, file = '../strength/l4t.str';  ! file with the magnet strength values

 BEAM, PARTICLE=Hminus, MASS=.93929, CHARGE=-1, PC=0.57083;! ENERGY=1.09929;

/*******************************************************************************
 * survey
 *******************************************************************************/
     surl4z : macro={
          use, sequence=l4z;
          XS0 = -1905.00325; YS0 = 2431.16062; ZS0 = 1946.16673; THETAS0 = -5.411594399884267;
          PHIS0   = 0.0;
          PSIS0   = 0.0;
          title, "l4z BEAM LINE - LS2 CONFIGURATION";
          set,  format="-18s";
          set,  format="15.9f";
          ! set, format="30.20f";
          select, flag=survey,clear;
          select, flag=survey, column=NAME,S,L,ANGLE,X,Y,Z,THETA,PHI,PSI,GLOBALTILT,SLOT_ID, ASSEMBLY_ID;
          if (ldbfiles == 1){
            survey, x0=XS0, y0=YS0, z0=ZS0, theta0=THETAS0, phi0=PHIS0, psi0=PSIS0, file="l4z_ldb_input_for_GEODE.sur";
            save,sequence=l4z,file="l4z_ldb_sav.seq",bare,noexpr;
            assign, echo="l4z_ldb_dumpsequ.txt";
            dumpsequ, sequence=l4z, level=1;
            assign, echo="terminal";
          } else {
            survey, x0=XS0, y0=YS0, z0=ZS0, theta0=THETAS0, phi0=PHIS0, psi0=PSIS0, file="l4z_input_for_GEODE.sur";
            save,sequence=l4z,file="l4z_sav.seq",bare,noexpr;
            assign, echo="l4z_dumpsequ.txt";
            dumpsequ, sequence=l4z, level=1;
            assign, echo="terminal";
          }
          
     }
     exec, surl4z;

! stop;
